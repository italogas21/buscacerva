export const modificaBusca = (texto) => {
    return {
        type: 'modifica_busca',
        payload: texto
    }
}