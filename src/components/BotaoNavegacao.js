import React from 'react';
import { Link } from "react-router-dom";
import Button from 'react-bootstrap/Button'
// import '../css/estilo.css';

function botaoNavegacao({ label, to }) {  
    return (
      <Button variant="dark" type="submit">
        <Link to={to}>{label}</Link>
      </Button>
    );
}
export default botaoNavegacao;