import React, { Component } from 'react';
import '../css/estilo.css';

class Detalhes extends Component {

    constructor(props) {
        super(props);
        this.addFavorito = this.addFavorito.bind(this);
        this.state = {id: props.item.id};
    }

    render() {
        return (
            <div id='area-principal'>
                <div id="descricao">
                    <h1 id='titulo'>{this.props.item.name}</h1>
                    <img className='img-descricao' src={this.props.item.image_url} alt="" style={{height: 320, width: 240}}></img> 
                    <p style={{fontSize: '24px', marginBottom: '5%'}}>{this.props.item.tagline}</p>
                    <p style={{marginBottom: '5%', textAlign: 'justify', textJustify: 'inter-word'}}>{this.props.item.description}</p>
                    <p><b>First Brewed: </b> {this.props.item.first_brewed}</p>
                    <p><b>ABV: </b>{this.props.item.abv}</p>
                    <p><b>IBU: </b>{this.props.item.ibu}</p>
                    <p><b>EBC: </b>{this.props.item.ebc}</p>
                    <p><b>SRM: </b>{this.props.item.srm}</p>
                    <p><b>Ph: </b>{this.props.item.ph}</p>
                    <p><b>Attenuation level: </b>{this.props.item.attenuation_level}</p>
                    <p><b>Volume: </b>{this.props.item.volume.value} {this.props.item.volume.unit}</p>
                    <p><b>Boil Volume: </b>{this.props.item.boil_volume.value} {this.props.item.boil_volume.unit}</p>
                    
                    <span className="postado"><b>Contributed by: </b> {this.props.item.contributed_by}</span>
                    <button className='addButton' onClick={this.addFavorito}>Adicionar aos favoritos.</button>
                </div>
            </div>
        );
    }

    addFavorito() {
        localStorage.setItem("chave" + JSON.stringify(this.state.id), JSON.stringify(this.state.id));
        alert("Cerveja: " + this.props.item.name + " adicionada aos favoritos. ");
    }
}

export default Cerveja;