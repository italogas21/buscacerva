import React from 'react';
import beer from '../beer.png';
import '../App.css';
import BarraNavegacao from './BarraNavegacao';
import BotaoNavegacao from './BotaoNavegacao';
import { connect } from 'react-redux';
import { modificaBusca } from '../actions/SearchActions'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'

const PaginaInicial = props =>  {
    return (
        <div className='principal'> 
            <BarraNavegacao></BarraNavegacao>
            <header className="header">
                <img src={beer} className="logo" alt="logo" />
                <h1 style={{ fontSize: 100 +'px'}}><span className="verde">Busca</span>Cerva</h1>
                <div style={{width: 50 + '%', borderWidth: 10 +'px'}}>
                    <InputGroup size="lg">
                        <FormControl aria-label="Large" aria-describedby="inputGroup-sizing-sm" type='input'
                                     onChange={texto => props.modificaBusca(texto.target.value)}/>
                        <InputGroup.Prepend>
                            <BotaoNavegacao to='/lista' label='Search'></BotaoNavegacao>
                        </InputGroup.Prepend>
                    </InputGroup>
                </div>
            </header>
        </div>
    );
}

const mapStateToProps = state => (
    {
        busca: state.SearchReducers.busca 
    }
);

export default connect(mapStateToProps, { modificaBusca } )(PaginaInicial);