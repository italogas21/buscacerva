import React, { Component } from 'react';
import axios from 'axios';
import Cerveja from './Cerveja'
import BarraNavegacao from './BarraNavegacao';
import '../App.css';

class Favoritos extends Component {

    constructor(props) {
        super(props);

        // this.handleNext = this.handleNext.bind(this);

        this.state = { listaDeCervejas: [], url: 'https://api.punkapi.com/v2/beers'};

        for(var i = 0; i<window.localStorage.length; i++) {
            var val = localStorage.getItem(localStorage.key(i))
            this.makeAPICall(this.state.url, val); 
        }

        console.log(this.state.listaDeCervejas);

    }

    render() {
        return (
            <div>
                <BarraNavegacao></BarraNavegacao>
                { this.state.listaDeCervejas.map( (item) =>  <Cerveja key={item.name} item={item}></Cerveja> ) }
            </div>
        );
    }

    makeAPICall(url, id) {
        url += `/${id}`;
        console.log('make api call: ' + url);
        axios.get(url)
            .then(response => { this.state.listaDeCervejas.push( response.data ); })
            .catch((e) => { console.log('Erro ao recuperar os dados: ' + e); });
    }
}

export default Favoritos;