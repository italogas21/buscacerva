import React from 'react';
// import BotaoNavegacao from './BotaoNavegacao'
import '../css/estilo.css';
import beer from '../beer.png';
import LoginForm from './LoginForm';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import Button from 'react-bootstrap/Button'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

function BarraNavegacao() {
    const [ modalShow, setModalShow ] = React.useState(false);
    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="#home">
                <img
                alt=""
                src={beer}
                width="30"
                height="30"
                className="d-inline-block align-top"
                />{' '}
                <span className="verde">Busca</span>Cerva
            </Navbar.Brand>
            <Nav className="ml-auto">
                <Nav.Item> 
                    {/* style={{padding: 2 + '%'}}> */}
                    <ButtonToolbar>
                        <Button style={{marginRight: 10 + 'px'}} variant="primary" onClick={() => setModalShow(true)}>
                            Log In
                        </Button>

                        <LoginForm
                            show={modalShow}
                            onHide={() => setModalShow(false)}
                        />
                    </ButtonToolbar>
                </Nav.Item>
                <Nav.Item>
                    <ButtonToolbar>
                        <Button variant="outline-primary" onClick={() => setModalShow(true)}>
                            Sign Up 
                        </Button>

                        <LoginForm
                            show={modalShow}
                            onHide={() => setModalShow(false)}
                        />
                    </ButtonToolbar>
                </Nav.Item>
            </Nav>
        </Navbar>

        // <div id="navegacao">
        //     <div id="area">
        //         <h1 id="logo"><span className="verde">Busca</span>Cerva</h1>
        //         <div id="menu">
        //             <ButtonToolbar>
        //                 <Button variant="primary" onClick={() => setModalShow(true)}>
        //                     LOG IN
        //                 </Button>

        //                 <LoginForm
        //                     show={modalShow}
        //                     onHide={() => setModalShow(false)}
        //                 />
        //             </ButtonToolbar>
        //             <BotaoNavegacao to='/login' label='Login'></BotaoNavegacao>
        //             <BotaoNavegacao to='/favoritos' label='Favoritos'></BotaoNavegacao>
        //         </div>
        //     </div>
        // </div>
    );
}

export default BarraNavegacao;