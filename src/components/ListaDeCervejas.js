import React, { Component } from 'react';
import axios from 'axios';
import Cerveja from './Cerveja'
import BarraNavegacao from './BarraNavegacao';
import '../App.css';
import { connect } from 'react-redux';
import { modificaBusca } from '../actions/SearchActions'

class ListaDeCervejas extends Component {

    constructor(props) {
        super(props);

        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);

        this.state = { listaDeCervejas: [], url: 'https://api.punkapi.com/v2/beers', page: 1 };

        if(this.props.aleatorio === 'true') {
            let url = this.state.url + '/random';
            this.makeAPICall(url, '');
        } else if(props.busca){
            console.log('Valor de props busca: ' + props.busca);
            this.makeAPICallUserInput(this.state.url, props.busca.toLowerCase());
        } else {
            this.makeAPICall(this.state.url, this.state.page);
        }

        console.log(this.state.url);
    }

    render() {
        return (
            <div>
                <BarraNavegacao></BarraNavegacao>
                { this.state.listaDeCervejas.map( (item) =>  <Cerveja key={item.name} item={item}></Cerveja> ) }
                <div className="botao_mais_resultados">
                    <button className='myButton' onClick={this.handlePrev}>Previous</button>
                    <button className='myButton' onClick={this.handleNext}>Next</button>
                </div>
            </div>
        );
    }

    makeAPICall(url, updatedPage) {
        let pageNumber = updatedPage ? `?page=${updatedPage}` : '';
        url += pageNumber;
        console.log('make api call: ' + url);
        axios.get(url)
            .then(response => { this.setState({ listaDeCervejas: response.data }); })
            .catch((e) => { console.log('Erro ao recuperar os dados: ' + e); });
    }

    makeAPICallUserInput(url, search) {
        url += `?beer_name=${search}`;
        console.log('make api call user: ' + url);
        axios.get(url)
            .then(response => { this.setState({ listaDeCervejas: response.data }); })
            .catch((e) => { console.log('Erro ao recuperar os dados: ' + e); });
    }

    handlePrev() {
        let nextPage = this.state.page - 1;
        this.setState({page: nextPage});
        this.makeAPICall(this.state.url, nextPage);
    }

    handleNext() {
        let nextPage = this.state.page + 1;
        this.setState({page: nextPage});
        this.makeAPICall(this.state.url, nextPage);
    }
}
const mapStateToProps = state => (
    {
        busca: state.SearchReducers.busca 
    }
);

export default connect(mapStateToProps, { modificaBusca } )(ListaDeCervejas);