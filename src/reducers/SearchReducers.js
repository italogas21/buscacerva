const INITIAL_STATE = {
    busca: ''
}

export default (state = INITIAL_STATE, action) => {
    if(action.type === 'modifica_busca') {
        return { ...state, busca: action.payload }
    }
    return state;
}