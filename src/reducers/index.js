import { combineReducers } from 'redux';
import SearchReducers from './SearchReducers';

export default combineReducers({
    SearchReducers : SearchReducers
});