import React, {Component} from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import ListaDeCervejas from './components/ListaDeCervejas';
import PaginaInicial from './components/PaginaInicial';
import Favoritos from './components/Favoritos'
import LoginForm from './components/LoginForm'
import reducers from './reducers'

class App extends Component {

  render() {
    return (
      <Provider store={createStore(reducers)}>
        <Router>
          <Switch>
            <Route exact path="/">
              <PaginaInicial />
            </Route>
            <Route exact path="/login">
              <LoginForm />
            </Route>
            <Route path="/lista">
              <ListaDeCervejas />
            </Route>
            <Route path="/aleatorio">
              <ListaDeCervejas aleatorio='true' />
            </Route>
            <Route path="/favoritos">
              <Favoritos favoritos='true' />
            </Route>
          </Switch> 
        </Router>
      </Provider> 
    );
  }
}

export default App;
