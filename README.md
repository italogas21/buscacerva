# BuscaCerva

A simple web app made using React/Redux for requesting beer catalog from [Punk API](https://punkapi.com/documentation/v2). Web app available [here](https://wizardly-spence-18b823.netlify.com).

## Features 
* Click 'Pesquisa no BuscaCerva' to request list of beers.
* Click 'Estou com sorte' to request ramdom beer.
* Select 'Adicionar aos favoritos' to save selected beer to localStorage.
* Click 'Favoritos' to see list of saved beers. 
